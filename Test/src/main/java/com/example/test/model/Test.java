package com.example.test.model;

import dk.framework.webframework.annotations.UrlMethod;
import dk.framework.webframework.utils.ModeleView;

public class Test {

    private int id;
    private String name;
    private int age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @UrlMethod(url = "test.dk")
    public ModeleView getList() {
        String list = "This is the list";
        String anotherData = "Hello " + name + ", you have " + age + " years old";
        ModeleView result = new ModeleView();
        result.put("list", list);
        result.put("other", anotherData);
        result.setUrl("test.jsp");
        return result;
    }
}
