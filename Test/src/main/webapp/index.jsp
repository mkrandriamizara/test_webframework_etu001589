<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>
<a href="test.dk">Test</a><br>
<a href="url.dk">Url not supported</a><br>
<form action="test.dk" method="post">
    <input type="text" name="name">
    <input type="number" name="age">
    <input type="submit" value="Submit">
</form>
</body>
</html>